import pandas as pd, sys
from argparse import ArgumentParser


def main():
    parser = ArgumentParser()
    parser.add_argument("-a", "--annotations",
                        help="Annotation file for open reading frames")
    parser.add_argument("-c", "--counts",
                        help="Counts file for open reading frames")
    parser.add_argument("-n", "--num_counts", type=int, default=500,
                        help="Minimum average count for genes across samples")
    parser.add_argument("-s", "--skip", nargs="*",
                        help="List of column names to ignore from mean calculations")

    args = parser.parse_args()

    # Read open reading frame (rows) counts in each sample (columns)
    counts = pd.read_csv(args.counts, header=0, sep="\t", index_col=0)
    # Remove samples if specified
    if args.skip:
        counts.drop(args.skip, axis=1, inplace=True)
    # Remove columns that do not have count data
    counts = counts.loc[:, counts.dtypes == int]

    # Read annotation file for open reading frames
    annots = pd.read_csv(args.annotations, header=None, sep="\t", index_col=0, names=["orf","gene"], usecols=[0,1])

    # Merge dataframes
    df = pd.merge(annots, counts, left_index=True, right_index=True)

    # Calculate sum of annotations in each sample
    dfsum = df.groupby("gene").sum()

    # Calculate mean sum of annotations across samples
    dfsum_mean = dfsum.mean(axis=1)

    # Keep only genes (rows) with mean sum >= args.num_counts
    dfkeep = dfsum_mean.loc[dfsum_mean>=args.num_counts]
    dfkeep.columns = ["gene","mean_sum"]

    # Write filtered data to stdout
    dfkeep.to_csv(sys.stdout, sep="\t")

if __name__ == '__main__':
    main()
